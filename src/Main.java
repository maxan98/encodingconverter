import java.io.*;

/**
 * Created by MaksSklyarov on 08.05.17.
 */
public class Main {
    public static void main(String[] args) {
        if(args.length != 4){
            System.out.println("DAUN");
            return;
        }
        String inf = args[0];
        String ouf = args[1];
        String cod1 = args[2];
        String cod2  = args[3];
        StringBuilder a = new StringBuilder();
        try {
            Reader reader = new InputStreamReader(
                    new FileInputStream(inf), cod1);
            Writer writer = new OutputStreamWriter(
                    new FileOutputStream(ouf), cod2);
            int c = 0;
            while ((c = reader.read()) >= 0) writer.write(c);
            reader.close();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Done");


    }
}
